//
//  NavigationPlugin.h
//  HelloWorld
//
//  Created by Jeavil_Tang on 2017/6/16.
//
//


//#import "CDV.h"
#import <Cordova/CDVPlugin.h>

@protocol TransitionDelegate;

@interface CDVNavigation : CDVPlugin

@property (nonatomic, readwrite, weak) id<TransitionDelegate> transitionDelegate;


- (void)goBack:(CDVInvokedUrlCommand *)command;

- (void)goBackWithReturnString:(CDVInvokedUrlCommand *)command;

- (void)redirect:(CDVInvokedUrlCommand *)command;

- (void)popPage:(CDVInvokedUrlCommand *)command;

- (void)deepLinkRedirect:(CDVInvokedUrlCommand *)command;


- (void)showFirstView:(CDVInvokedUrlCommand *)command;

- (void)showSceondView:(CDVInvokedUrlCommand *)command;

- (void)backToFirstView:(CDVInvokedUrlCommand *)command;

@end
