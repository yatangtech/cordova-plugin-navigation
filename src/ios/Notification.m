//
//  Notification.m
//  HelloWorld
//
//  Created by Jeavil_Tang on 2017/6/23.
//
//

#import "Notification.h"

@implementation Notification

- (void)emit:(CDVInvokedUrlCommand *)command {
    NSString *module_id = [command argumentAtIndex:0 withDefault:nil];
    NSString *key = [command argumentAtIndex:1 withDefault:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"H5Notification" object:@{@"moduleID":module_id, @"key":key}];
}

@end
