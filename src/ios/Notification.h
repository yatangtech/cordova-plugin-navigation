//
//  Notification.h
//  HelloWorld
//
//  Created by Jeavil_Tang on 2017/6/23.
//
//

#import <Cordova/CDVPlugin.h>

@interface Notification : CDVPlugin

- (void)emit:(CDVInvokedUrlCommand *)command;

@end
