//
//  NavigationPlugin.m
//  HelloWorld
//
//  Created by Jeavil_Tang on 2017/6/16.
//
//

#import "CDVNavigation.h"
#import "MainViewController.h"

@interface CDVNavigation ()
@property (nonatomic, strong) CDVInvokedUrlCommand *command;
@property (nonatomic, strong) CDVViewController *mainVC;
@end

@implementation CDVNavigation

- (void)goBack:(CDVInvokedUrlCommand *)command {
    NSArray *viewcontrollers=self.viewController.navigationController.viewControllers;
    if (viewcontrollers.count>1) {
        [self.viewController.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self.viewController dismissViewControllerAnimated:YES completion:nil];
    }
}


- (void)goBackWithReturnString:(CDVInvokedUrlCommand *)command {
//    NSString *returnStringData = [command argumentAtIndex:0 withDefault:nil];
    [self.viewController.navigationController popViewControllerAnimated:YES];
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:nil];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];

}

- (void)redirect:(CDVInvokedUrlCommand *)command {
    NSString *url = [command argumentAtIndex:0 withDefault:nil];
    NSDictionary *option = [command argumentAtIndex:1 withDefault:nil];
    NSString *transition = option[@"transition"];
//    NSString *pageReturnParams = option[@"pageReturnParams"];
    CDVViewController *vc = [[CDVViewController alloc] init];
    NSString *curFilePath=[NSString stringWithFormat:@"file://%@/www",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
    NSLog(@"路径为:%@",curFilePath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:curFilePath]) {
        vc.wwwFolderName = curFilePath;
    }
    vc.startPage = [NSString stringWithFormat:@"%@/%@",curFilePath,url];

    if ([transition isEqualToString:@"bottom"]) {
//        [UIView  beginAnimations:nil context:NULL];
//        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//        [UIView setAnimationDuration:0.35];
//        [self.viewController.navigationController pushViewController:vc animated:NO];
//        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.viewController.navigationController.view cache:NO];
//        [UIView transitionFromView:self.viewController.navigationController.view toView:vc.view duration:0.35
//                           options:UIViewAnimationOptionTransitionFlipFromTop completion:nil];
//        [UIView commitAnimations];

        [self.viewController presentViewController:vc animated:YES completion:nil];
    }else {
        [self.viewController.navigationController pushViewController:vc animated:YES];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationHandle:) name:@"H5Notification"  object:nil];
}

- (void)popPage:(CDVInvokedUrlCommand *)command {
    NSString *url = [command argumentAtIndex:0 withDefault:nil];
    for (UIViewController *vc in self.viewController.navigationController.viewControllers) {
        if ([vc isKindOfClass:[CDVViewController class]]) {
            NSString *originUrl = ((CDVViewController *)vc).startPage;
            NSLog(@"----> url = %@",originUrl);
            if (originUrl == url) {
                [self.viewController.navigationController popToViewController:vc animated:YES];
            }else {
                CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_CLASS_NOT_FOUND_EXCEPTION];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }
        }
    }
}


- (void)deepLinkRedirect:(CDVInvokedUrlCommand *)command {
    NSDictionary *option = [command argumentAtIndex:0 withDefault:nil];
    NSString *url = option[@"url"];
//    NSString *title = option[@"title"];
//    NSDictionary *parmas = option[@"params"];
    CDVViewController *vc = [[CDVViewController alloc] init];
    NSString *curFilePath=[NSString stringWithFormat:@"file://%@/www",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
    NSLog(@"路径为:%@",curFilePath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:curFilePath]) {
        vc.wwwFolderName = curFilePath;
    }
    vc.startPage = url;
    
    [self.viewController.navigationController popToViewController:vc animated:YES];
}

- (void)notificationHandle:(NSNotification *)noti {
    id obj = noti.object;
    NSLog(@"接收到的广播内容 = %@",obj);
}


/** --------------------------------------------------------------------------------------- **/

- (void)showFirstView:(CDVInvokedUrlCommand *)command {
    self.command = command;
    CDVViewController *firstVC = [[CDVViewController alloc] init];
    firstVC.title = @"界面一";
    [firstVC configDefaultNavigationbar];
    [firstVC customRightItemNormalImage:nil highLightedImage:nil title:@"帮助"];
    
    NSString *curFilePath=[NSString stringWithFormat:@"file://%@/www",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
    NSLog(@"路径为:%@",curFilePath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:curFilePath]) {
        firstVC.wwwFolderName = curFilePath;
    }
    firstVC.startPage=@"page1.html";
    [self.viewController.navigationController pushViewController:firstVC animated:YES];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        CDVViewController *firstVC = [[CDVViewController alloc] init];
//        firstVC.title = @"界面一";
//        NSString *curFilePath=[NSString stringWithFormat:@"file://%@/www",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
//        NSLog(@"路径为:%@",curFilePath);
//        if ([[NSFileManager defaultManager] fileExistsAtPath:curFilePath]) {
//            firstVC.wwwFolderName = curFilePath;
//        }
//        firstVC.startPage=@"page1.html";
//        [self.viewController.view addSubview: firstVC.view];
//    });
}

- (void)showSceondView:(CDVInvokedUrlCommand *)command {
    self.command = command;
    CDVViewController *sceondVC = [[CDVViewController alloc] init];
    sceondVC.title = @"界面二";
    [sceondVC configDefaultNavigationbar];
    [sceondVC customRightItemNormalImage:nil highLightedImage:nil title:@"首页"];
    
    NSString *curFilePath=[NSString stringWithFormat:@"file://%@/www",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
    NSLog(@"路径为:%@",curFilePath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:curFilePath]) {
        sceondVC.wwwFolderName = curFilePath;
    }
    sceondVC.startPage=@"page2.html";
    [self.viewController.navigationController pushViewController:sceondVC animated:YES];
}


- (void)backToFirstView:(CDVInvokedUrlCommand *)command {
    [self.viewController.navigationController popToRootViewControllerAnimated:YES];
}

- (void)rightBarItemAction {
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
    if ([currentVC.title isEqualToString:@"界面二"]) {
        [self.viewController.navigationController popToRootViewControllerAnimated:YES];
    }else {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"帮助"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:_command.callbackId];
    }
}

- (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC {
    
    UIViewController *currentVC;
    
    if ([rootVC presentedViewController]) {
        // 视图是被presented出来的
        
        rootVC = [rootVC presentedViewController];
    }
    
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
        
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
        
    } else {
        // 根视图为非导航类
        
        currentVC = rootVC;
    }
    
    return currentVC;
}


@end
