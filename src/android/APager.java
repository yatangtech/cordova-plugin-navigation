package org.apache.cordova.plugins;

import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;

import com.google.gson.Gson;

import org.apache.cordova.CordovaActivity;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * This class echoes a string called from JavaScript.
 */
public class APager extends CordovaPlugin {

    private static FragmentManager fragmentManager;
    private static int containerid;

    private static CordovaActivity activity;

    public static void init(FragmentManager fragmentManager,int containerid){
        APager.fragmentManager = fragmentManager;
        APager.containerid = containerid;
    }

    public static void init(CordovaActivity activity){
        APager.activity = activity;
    }

    public static void destroy(){
        APager.fragmentManager = null;
    }

    @Override
    public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException {
        if(fragmentManager == null || fragmentManager.isDestroyed()) return false;
        switch (action){
            case "redirect":
                break;
            case "goBack":
                break;
            case "notify":
                ActivityManager.registerGlobalNotfiy(callbackContext);
                break;
        }
        return super.execute(action, args, callbackContext);
    }

    @Override
    public boolean execute(String action, String rawArgs, CallbackContext callbackContext) throws JSONException {
        Gson gson = new Gson();
        Pages pages = gson.fromJson(rawArgs,Pages.class);
        if(pages != null){
            String path = "file://"+ Environment.getExternalStorageDirectory().getPath()+"/www/"+pages.url;
            switch (action){
                case "redirect":
                    Bundle bundle = new Bundle();
                    bundle.putString("path",path);
                    bundle.putString("name",pages.name);
                    bundle.putString("data",pages.data);
                    bundle.putString("anim",pages.anim);
                    ActivityManager.skip(pages.name,bundle,callbackContext);
                   break;
                case "goBack":
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("data",pages.data);
                    ActivityManager.goBack(bundle1);
                    break;
                case "getdata":
                    callbackContext.success(ActivityManager.currentData());
                    break;
            }
        }
        return true;
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        return super.execute(action, args, callbackContext);
    }

    public class Pages{
        public String name;
        public String path;
        public String data;
        public String anim;
        public String url;
    }
}
