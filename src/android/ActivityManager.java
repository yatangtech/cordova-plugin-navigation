package org.apache.cordova.plugins;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.yatang.test.R;

import org.apache.cordova.CallbackContext;

import java.util.LinkedHashMap;
import java.util.LinkedList;

/**
 * Created by liaoqinsen on 2017/6/22 0022.
 */

public class ActivityManager {
    public static final LinkedList<SingleTypeActivity> activitys;
    public static final LinkedHashMap<SingleTypeActivity,CallbackContext> callbackContexts;
    private static SingleTypeActivity current;
    private static Handler handler;

    private static CallbackContext callbackContext;

    public static  void registerGlobalNotfiy(CallbackContext call){
        callbackContext = call;
    }

    static {
        activitys = new LinkedList<>();
        callbackContexts = new LinkedHashMap<>();
    }

    public static void current(SingleTypeActivity activity){
        if(current != null) {
            activitys.push(current);
        }
        current = activity;
    }

    public static String currentData(){
        return current.getData();
    }

    public static boolean isFirst(){
        return current == null;
    }

    public static void init(){
        if(handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
    }

    public static void skip(Class<SingleTypeActivity> activityClass){
        skip(activityClass,null);
    }

    public static void skip(Class<SingleTypeActivity> activityClass, Bundle bundle){
        if(current == null) return;
        Intent intent = new Intent(current,activityClass);
        if(bundle != null) {
            intent.putExtras(bundle);
        }
        current.startActivity(intent);
    }


    public static void skip(String name, final Bundle bundle, CallbackContext call){
        if(current == null) return;
        callbackContexts.put(current, call);
        if(name != null) {
            boolean has = false;
            for (SingleTypeActivity singleTypeActivity : activitys) {
                if (name.equals(singleTypeActivity.getName())){
                    has = true;
                    break;
                }
            }
            if(has){
                while(activitys.size() > 0){
                    if(name.equals(current.getName())){
                        String data = bundle.getString("data");
                        if(data != null) {
                            CallbackContext callbackContext = callbackContexts.get(current);
                            if(callbackContext != null) callbackContext.success(data);
                        }
                        return;
                    }
                    current.finishByManager();
                    callbackContexts.remove(current);
                    current = activitys.pop();
                }
                return;
            }
        }
        final Intent intent = new Intent(current,SingleTypeActivity.class);
        if(bundle != null) {
            intent.putExtras(bundle);
        }
        String anim = bundle.getString("anim");
        if(anim != null) {
            switch (anim) {
                case "right":
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            intent.putExtra("in", R.anim.slide_in_right);
                            intent.putExtra("out",R.anim.slide_out_right);
                            current.startActivity(intent);
                            current.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                        }
                    });
                    break;
                case "bottom":
                    handler.post(new Runnable() {
                    @Override
                    public void run() {
                        intent.putExtra("in",R.anim.slide_in_bottom);
                        intent.putExtra("out",R.anim.slide_out_bottom);
                        current.startActivity(intent);
                        current.overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
                    }
                });
                    break;
                default:
                    current.startActivity(intent);
                    break;
            }
        }else{
            current.startActivity(intent);
        }
    }

    public static void skipForResult(Class<SingleTypeActivity> activityClass, Bundle bundle, CallbackContext callbackContext){
        if(current == null) return;
        Intent intent = new Intent(current,activityClass);
        if(bundle != null) {
            intent.putExtras(bundle);
        }
        callbackContexts.put(current,callbackContext);
        current.startActivityForResult(intent,100);
    }

    public static void goBack(){
        goBack(null);
    }

    public static void goBack(Bundle data){
        if(current == null) return;
        current.finishByManager();
        callbackContexts.remove(current);
        if(activitys.size() > 0) {
            current = activitys.pop();
        }else{
            current = null;
        }
        if(data == null) return;
        CallbackContext callbackContext = callbackContexts.get(current);
        String d = data.getString("data");
        if(d != null && callbackContext != null) {
            callbackContext.success(d);
        }
    }
}
