package org.apache.cordova.plugins;

import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.webkit.WebView;

import org.apache.cordova.CordovaActivity;
import org.crosswalk.engine.XWalkCordovaView;

/**
 * Created by liaoqinsen on 2017/6/22 0022.
 */

public class SingleTypeActivity extends CordovaActivity {

    private String name,data;

    private int in,out;

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityManager.init();
        if(getIntent() != null && getIntent().getExtras() != null){
            name = getIntent().getExtras().getString("name");
            in = getIntent().getExtras().getInt("in");
            out = getIntent().getExtras().getInt("out");
            data = getIntent().getExtras().getString("data");
        }
        String path = null;
        if (getIntent() != null && getIntent().getExtras() != null) {
            path = getIntent().getExtras().getString("path");
        }
        if(ActivityManager.isFirst()){
            path = "file://"+ Environment.getExternalStorageDirectory().getPath()+"/www/"+"/pagea.html";
        }
        if(path != null) {
            loadUrl(path);
            View view = appView.getView();
            if(view instanceof WebView){
                ((WebView)view).getSettings().setUseWideViewPort(true);
            }else if(view instanceof XWalkCordovaView){
                ((XWalkCordovaView)view).getSettings().setUseWideViewPort(true);
            }
        }else{
            android.widget.Toast.makeText(this,"找不到HTML文件", android.widget.Toast.LENGTH_SHORT).show();
            ActivityManager.goBack();
        }

        ActivityManager.current(this);
    }

    public String getData(){
        return  data;
    }

    @Override
    public void finish() {
        ActivityManager.goBack();
    }

    public void finishByManager(){
        if(in > 0 && out > 0) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    finishInternal();
                    overridePendingTransition(in, out);
                }
            });
        }else {
            finishInternal();
        }
    }

    private void finishInternal(){
        super.finish();
    }

    @Override
    public void onBackPressed() {
        ActivityManager.goBack();
    }
}
